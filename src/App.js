
import './App.css';
import LapButton from './components/LapButton';
import ResetButton from './components/ResetButton';
import StartButton from './components/StartButton';
import StopButton from './components/StopButton';
import StopWatch from './components/StopWatch';

function App() {
  return (
    <div className='App'>
      <StopWatch/>
      <StartButton/>
      <StopButton/>
      <ResetButton/>
      <LapButton/>
      
    </div>
  );
}

export default App;
