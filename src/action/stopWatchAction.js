

export const start=()=>{
    return{
        type:"START_TIMER",
    }
}
export const milliSecond=()=>{
    return{
        type:"SET_MILLISECOND",
    }
}
export const second=()=>{
    return{
        type:"SET_SECOND",
    }
    
}
export const minute=()=>{
    return {
        type:"SET_MINUTE",
    }
}
export const hour=()=>{
    return{
        type:"SET_HOUR",
    }
}
export const setMilliSecondToZero=()=>{
    return {
        type:"SET_MILLISECOND_TO_ZERO",
    }
}
export const setSecondToZero=()=>{
    return{
        type:"SET_SECOND_TO_ZERO",
    }
}
export const setMinuteToZero=()=>{
    return{
        type:"SET_MINUTE_TO_ZERO",
    }
}
export const pauseTimer=()=>{
    return{
        type:"PAUSE_TIMER",
    }
}
export const resetTimer=()=>{
    return{
        type:"RESET_TIMER",
    }
}
export const addLap=(payload)=>{
    return{
        type:"ADD_LAP",
        payload:payload,
    }
}