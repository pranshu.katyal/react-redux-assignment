import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { start,milliSecond,second,minute,hour,setMilliSecondToZero,setSecondToZero,setMinuteToZero,pauseTimer,resetTimer,addLap } from '../action/stopWatchAction';

const StopWatch = () => {
let dispatch=useDispatch();

let milliSecond=useSelector((state)=>state.milliSecond);
let second=useSelector((state)=>state.second);
let minute=useSelector((state)=>state.minute);
let hour=useSelector((state)=>state.hour);
let isStarted=useSelector((state)=>state.isStarted);
let pause=useSelector((state)=>state.pause);

useEffect(()=>{
    if(isStarted==true){
        let interval=setInterval(() => {
            if(!pause){
                dispatch({type:"SET_MILLISECOND"});
            }
            if(milliSecond/99===1){
                dispatch(setMilliSecondToZero());
                dispatch({
                    type:"SET_SECOND",
                });
            }
            if(second/60===1){
                dispatch(setSecondToZero());
                dispatch(minute());
            }
            if(minute/60===1){
                dispatch(setMinuteToZero);
                dispatch(hour());
            }


        }, 10);
        return ()=>{
            window.clearInterval(interval);
        }

    }
},[milliSecond,second,minute,hour,pause,isStarted])

return(
    <div className='timer'>
        <h2>
            <span>{hour<10?"0"+hour:hour}:</span>
            <span>{minute<10?"0"+minute:minute}:</span>
            <span>{second<10?"0"+second:second}:</span>
            <span>{milliSecond<10?"0"+milliSecond:milliSecond}</span>
        </h2>
    </div>
)


}
 
export default StopWatch;