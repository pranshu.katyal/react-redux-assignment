import React from "react";
import { useDispatch, useSelector } from "react-redux";

const LapButton = () => {
    let dispatch=useDispatch();
    let {pause,isStarted}=useSelector((state)=>state);
    let {hour,minute,second,milliSecond,laps}=useSelector((state)=>state);
    let lapData={
      num:laps.length+1,
      hour,
      minute,
      second, 
      milliSecond,
    }

    const handleClick=()=>{
        dispatch({type:"ADD_LAP",payload:lapData})
    }

    return (  
        <>
            {isStarted && !pause ?(<button className="lap-btn" onClick={()=>{handleClick()}}>Lap</button>):("") }
        </>
    );
}
 
export default LapButton;

