import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { start } from "../action/stopWatchAction";

const StartButton = () => {
    let dispatch=useDispatch();
    let {isStarted}=useSelector((state)=>state);
    const handleClick=()=>{
        dispatch(start());
    }
    return (
        <>
            {!isStarted ? (<button className="btn" onClick={()=>{handleClick();}}>start</button>):("")}
        </>
    )

   
}
 
export default StartButton;