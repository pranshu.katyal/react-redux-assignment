import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { resetTimer } from "../action/stopWatchAction";


const ResetButton = () => {
    let dispatch=useDispatch();
    let {pause,isStarted}=useSelector((state)=>state);
    const handleClick=()=>{
        dispatch(resetTimer());
    }
    return (
        <>
            {isStarted && pause? (<button className="reset-btn" onClick={()=>{handleClick()}}>Reset</button>):("")}
        </>
      );
}
 
export default ResetButton;