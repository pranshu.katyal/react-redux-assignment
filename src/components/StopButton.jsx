import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { pauseTimer } from "../action/stopWatchAction";

const StopButton = () => {
    let dispatch=useDispatch();
    let {pause,isStarted}=useSelector((state)=>state);
    const handleClick=()=>{
        dispatch(pauseTimer());
        let StopBtn = document.querySelector(".StopBtn");
    }

    return (  
        <>
            {isStarted ? (<button className="StopBtn" onClick={()=>{handleClick();}}>
                {pause ?"Resume":"Stop"}
            </button>):("")}
        </>
       
    );
}
 
export default StopButton;