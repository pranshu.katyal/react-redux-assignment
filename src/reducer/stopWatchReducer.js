let initialState={
    isStarted:false,
    milliSecond:0,
    second:0,
    minute:0,
    hour:0,
    pause:false,
    laps:[],
}

export const stopWatchReducer=(state=initialState,action)=>{
    switch(action.type){
        case "START_TIMER":
            // logic
            return {...state,isStarted:!state.isStarted};

        case "SET_MILLISECOND":
            //LOGIC
            return {...state,milliSecond:state.milliSecond+1}
        case "SET_SECOND":
            // LOGIC
            return {...state,second:state.second+1};

        case "SET_MINUTE":
            //LOGIC
            return {...state,minute:state.minute+1};
        case "SET_HOUR":
            //LOGIC
            return {...state,hour:state.hour+1};
        case "SET_MILLISECOND_TO_ZERO":
            //LOGIC
            return {...state,milliSecond:0};
        case "SET_SECOND_TO_ZERO":
            // LOGIC
            return {...state,second:0};
        case "SET_MINUTE_TO_ZERO":
            //LOGIC
            return {...state,minute:0};
        case "PAUSE_TIMER":
            //LOGIC
            return {...state,pause:!state.pause}
        case "RESET_TIMER":
            //LOGIC
            return{
                isStarted:false,
                milliSecond:0,
                second:0,
                minute:0,
                hour:0,
                pause:false,
                laps:[],
            }
        
        case "ADD_LAP":
            //LOGIC

        default:
            return state;
    }
}