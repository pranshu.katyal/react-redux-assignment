import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { stopWatchReducer } from './reducer/stopWatchReducer';

import './index.css';
import App from './App';
import { Provider } from 'react-redux';

let store=createStore(stopWatchReducer);



ReactDOM.render(
  <Provider store={store}>

    <App />,
  </Provider>,
  document.getElementById('root')
);



